import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1And2 {
    public static void main(String[] args) {
        String loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";

        String result = loremIpsum.replace("L", "7").replace("i", "1")
                .replace("p", "8").replace("o", "9").replace("s", "5");

        System.out.println(result);

        String numericalCharacters = extractCharacters(result, "\\d");
        String alphabeticalCharacters = extractCharacters(result, "[a-zA-Z]");

        writeToFile("821.txt", numericalCharacters);
        writeToFile("822.txt", alphabeticalCharacters);

    }

    private static String extractCharacters(String input, String regex) {
        StringBuilder result = new StringBuilder();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            result.append(matcher.group());
        }

        return result.toString();
    }

    private static void writeToFile(String filename, String content) {
        try (FileWriter writer = new FileWriter(filename)) {
            writer.write(content);
            System.out.println("Successfully wrote to " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
