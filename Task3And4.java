import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Task3And4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Get user input for registration day
        System.out.print("Enter the registration date (yyyy-MM-dd): ");
        String registrationDateStr = scanner.nextLine();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date registrationDate = dateFormat.parse(registrationDateStr);

            // Get user input for the number of days before payment day for reminders
            System.out.print("Enter how many days before payment day you want to be reminded (1-7): ");
            int reminderDays = Integer.parseInt(scanner.nextLine());

            System.out.print("New user (y/n)? ");

            boolean newUser = (scanner.nextLine()).equals("y");

            // Create and write to netflix.txt
            writeNetflixPaymentsAndReminders(registrationDate, reminderDays, newUser);

            System.out.println("Netflix payments and reminders recorded in netflix.txt.");

        } catch (Exception e) {
            System.err.println("Invalid date format. Please use yyyy-MM-dd.");
        } finally {
            scanner.close();
        }
    }

    private static void writeNetflixPaymentsAndReminders(Date registrationDate, int reminderDays, boolean newUser) {
        try (FileWriter writer = new FileWriter("netflix.txt")) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(registrationDate);

            boolean registered = false;

            for (int month = 1; month <= 12; month++) {
                if (registered) {
                    // Reminder
                    calendar.add(Calendar.DAY_OF_MONTH, -reminderDays);
                    writer.write(formatDate(calendar.getTime()) + " - Reminder email sent to user\n");
                    calendar.add(Calendar.DAY_OF_MONTH, reminderDays);
                }

                // Payment day
                writer.write(formatDate(calendar.getTime()) + " - payment - "
                        + (newUser && (month == 2 || month == 3 || month == 4) ? "Free\n" : "Rp. 153.000\n"));
                registered = true;

                // Move to the next month
                calendar.add(Calendar.MONTH, 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }
}
